import menu.*;
import metier.*;
import commande.*;

public class Demo
{
    
    public static void demo_metier()
    {
        
        // Créer banque, personne, compte associé aux deux
        Banque bnp = new Banque("BNP");
        System.out.println(bnp);
                
        Personne paulo = new Personne("Paulo");
        System.out.println(paulo);
               
        bnp.ajouteClient(paulo);
              
        CompteBancaire cptChq = new CompteCheque("0001", 100., bnp, paulo);
        System.out.println(cptChq);

        CompteBancaire cptEp = new CompteEpargne("0002", 100., bnp, paulo, 0.75);
        System.out.println(cptEp);
        
    }
    
    public static void demo_menu() {
        Menu menu = new Menu();
        MenuOption opt1 = new MenuOption("a","Ajouter un compte au client",
            new AjouterCompteCmd());
        MenuOption opt2 = new MenuOption("c","Lister les comptes du client",
            new ListerComptesDuClientCmd());
        menu.ajouterOption(opt1);
        menu.ajouterOption(opt2);
        menu.boucler();
    }
}
