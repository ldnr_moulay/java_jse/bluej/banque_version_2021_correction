package menu;
import java.util.List;
import java.util.ArrayList;
import io.*;

public class Menu
{
    private List<MenuOption> options;
    InputScanner clavier = new InputScanner();
    
    public Menu() {
        options = new ArrayList<>();
    }
    
    public void afficher() {
        Ecran.efface();
        for (MenuOption option : options) {
            option.afficher();
        }
    }
    
    public void ajouterOption(MenuOption option) {
        options.add(option);
    }
    
    /**
     * Retourne le MenuOption qui correspond au raccourci s
     */
    private MenuOption optionPour(String s) {
        MenuOption optionChoisie = null;
        for (MenuOption option : options) {
            if (option.getRaccourci().equals(s)) {
                optionChoisie = option;
                break;
            }
        }
        return optionChoisie;
    }
    
    /**
     * Gère le menu
     */
    public void boucler() {
        boolean fini = false;
        while(! fini) {
            afficher();
            String choix = clavier.saisirMinuscule();
            if (choix.equals("q"))
                fini = true;
            else {
                MenuOption o = optionPour(choix);
                if (o != null)
                    o.getCommande().executer();
            }
        }
    }
}
