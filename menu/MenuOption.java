package menu;

import commande.Commande;

public class MenuOption
{
    private String label;
    private String raccourci;
    private Commande commande;
    
    public MenuOption(String raccourci, String label, Commande cmd) {
        this.raccourci = raccourci;
        this.label = label;
        commande = cmd;
    }

    public void afficher() {
        System.out.println("[" + raccourci + "] " + label);
    }
    
    public String getRaccourci() {
        return raccourci;
    }
    
    public Commande getCommande() {
        return commande;
    }
}
