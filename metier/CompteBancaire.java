package metier;

import java.util.*;

/**
 * 
 */
public abstract class CompteBancaire {
    private String numeroCompte;
    private double solde;
    private Banque banque;
    private Personne detenteur;
    
    /**
     * Constructeur
     */
    public CompteBancaire(String numeroCompte, double solde,
        Banque banque, Personne detenteur) {
        this.numeroCompte = numeroCompte;
        this.solde = solde;
        this.banque = banque;
        this.setDetenteur(detenteur);
        
    }
    
    public void setDetenteur(Personne detenteur) {
        this.detenteur = detenteur;
        if (! detenteur.detientCompte(this))
            detenteur.ajouteCompte(this);
    }
    
    public Personne getDetenteur() {
        return detenteur;
    }

    @Override
    public String toString() {
        return "Compte n° " + numeroCompte
            + ", solde : " + solde
            + ", domicilié à " + banque.getNom()
            + ", détenu par " + detenteur.getNom();
    }
    
    /**
     * @param montant
     */
    public void crediter(double montant) {
        assert montant > 0;
        solde += montant;
    }

    /**
     * @param montant
     */
    public void debiter(double montant) {
        assert montant > 0;
        solde -= montant;
    }
    
    public void setBanque(Banque banque) {
        this.banque = banque;
    }   

}