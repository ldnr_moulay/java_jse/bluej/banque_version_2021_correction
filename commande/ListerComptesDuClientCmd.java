package commande;

import metier.*;
import io.*;

public class ListerComptesDuClientCmd extends Commande
{
    InputScanner inputScanner = new InputScanner();

    @Override
    public void executer() {
        System.out.println("Je liste les comptes du client");
        inputScanner.typeEnter();

    }
}
