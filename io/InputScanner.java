package io;
import java.util.Scanner;

public class InputScanner
{
    static Scanner reader = new Scanner(System.in);
    
    public String saisirMinuscule() {
        afficherInvite();
        return reader.nextLine().toLowerCase();
    }
    
    private void afficherInvite() {
        System.out.print("> ");
    }
    
    public void typeEnter() {
        System.out.println("Appuyez sur Entrée pour continuer...");
        saisirMinuscule();
    }
}
